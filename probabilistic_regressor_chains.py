import warnings
warnings.filterwarnings("ignore")

from sklearn.base import RegressorMixin, MultiOutputMixin, BaseEstimator
from sklearn.linear_model import BayesianRidge
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.preprocessing import KBinsDiscretizer
from sklearn.neighbors import KernelDensity as KDE
from sklearn.metrics.pairwise import rbf_kernel

from utils_metrics import weighted_median

import numpy as np
from scipy.stats import norm
import copy

class F_BayesianRidge(BayesianRidge):
    ''' A wrapper around Bayesian regression.
        
        Provide sample and evaluate routines, as required by Monte Carlo
        probabilistic regressor chain methods.
    '''

    def fit(self,X,y,sample_weight=None):
        super().fit(X,y)

    def sample(self,X, random_state=None):
        ''' Get a conditional sample.

        Given inputs `X`, return samples y, where y[i] ~ p(Y|X=X[i]). 
        (i.e., the samples are in the output space). 

        returns an array of length N. 
        '''
        m, s = self.predict(X,return_std=True)
        return m + np.random.randn(X.shape[0]) * s 

    def evaluate(self,X,y):
        ''' Evaluate p(Y=y|X=x) for all rows in X and y.

        N.B. This is similar to the `score` function.

        returns an array of length N.
        '''
        m, s = self.predict(X,return_std=True)
        return norm.pdf(y, m, s)

    def sample_eval(self,X, random_state=None):
        ''' Get conditional samples and evaluate them. 

        returns two arrays, each of length N:
            y ~ p(Y|X=x), and their weights, 
            w = p(Y=y|X=x)
            for each row x in X.
        N.B., this method is a combination of `sample` and `evaluate` (for efficiency reasons). 
        '''
        m, s = self.predict(X,return_std=True)
        y = m + np.random.randn(X.shape[0]) * s 
        return y, norm.pdf(y, m, s)

    def get_density(self,x):
        ''' Get a conditional pdf.

        Return a function `f_x`, which is a pdf conditioned on `x`, such that 
        we may call `plot(y,f(y))` for some numpy array `y` (over a range) to
        draw the density. 

        Note that `x` should be a *single* sample/instance.
        '''
        m, s = self.predict(x.reshape(1,-1),return_std=True)
        def f_x(z):
            N = len(z)
            p_z = np.zeros((N))
            for i in range(N):
                p_z[i] = norm.pdf(z[i], m, s)[0]
            return p_z
        return f_x

def _kfit(y,w,gamma):
    ''' Fit a KDE on y, respective of weights w, bandwidth gamma. '''
    if np.sum(w) <= 0:
        w = np.ones * 1./len(w)
    f = KDE(kernel='gaussian', bandwidth=gamma)
    f.fit(y.reshape(-1,1),sample_weight=w)
    return f

class F_Kernel():

    def __init__(self, kernel='rbf', gamma=0.3):
        self.kernel = kernel
        self.gamma = gamma

    def fit(self, X, y=None, sample_weight=None):
        self.y_store = y
        self.x_store = X

    def _sample (self, ytrain, omega, random_state=None):
        omega = omega**40/sum(omega**40) 
        i = np.random.choice(np.arange(ytrain.shape[0]),p=omega.flatten())
        mu = ytrain[i]
        sd = self.gamma * 0.1
        #f = _kfit(self.y_store,omega,self.gamma)
        #y[i] = f.sample(n_samples=1,random_state=random_state)
        return mu + np.random.randn() * sd

    def sample(self,X, random_state=None):
        N, D = X.shape
        K = rbf_kernel(X,self.x_store,gamma=self.gamma)
        y = np.zeros(N)
        for i in range(N):
            y[i] = self._sample(self.y_store,K[i,:])
        return y

    def _evaluate(self,omega,y):
        f = _kfit(self.y_store,omega,gamma=self.gamma)
        #p[i] = norm.pdf(y[i], mu, s)
        return np.exp(f.score(y.reshape(1,-1)))

    def evaluate(self,X,y):
        N, D = X.shape
        K = rbf_kernel(X,self.x_store,gamma=self.gamma)
        p = np.zeros(N)
        for i in range(N):
            p[i] = self._evaluate(K[i,:],y[i].reshape(1,1))
        return p

    def sample_eval(self,X,random_state=None):
        # N = number of test examples = number of samples to take
        N, D = X.shape
        # Form K: each i-th row is the weight vector w_i wrt X[i]
        K = rbf_kernel(X,self.x_store,gamma=self.gamma)
        # Sample
        y = self.sample(X, random_state=random_state)
        # Eval.
        p = self.evaluate(X,y)
        #y[i] = self._sample(X, K[i,:])
        #p[i] = self._evaluate(X, y[i])
            # Evaluate
            #p[i] = omega @ self.y_store
        #p = np.exp(f.score(y.reshape(-1,1)))
        #print(p)
        return y, p

    def get_density(self,x):
        def f_x(z):
            N = len(z)
            p_z = np.zeros((N))
            K = rbf_kernel(x.reshape(1,-1),self.x_store,gamma=self.gamma)
            f = _kfit(self.y_store,w=K.ravel(),gamma=self.gamma)
            for i in range(N):
                p_z[i] = np.exp(f.score(z[i].reshape(-1,1)))
            return p_z
        return f_x


class F_Histogram_Classifier(BaseEstimator):

    k = -1
    h = None

    def __init__(self, base_learner=ExtraTreesClassifier(), k = 10):
        self.k = k
        self.h = base_learner

    def _cross_val_test(self, X, y):
        return


    def fit(self, X, y):
        # Discretize Y
        self.discretizer = KBinsDiscretizer(n_bins=self.k, encode='ordinal', strategy='kmeans')
        self.discretizer.fit(y.reshape(-1,1))
        c = self.discretizer.transform(y.reshape(-1,1)).ravel()
        # Standard classifier training
        self.h.fit(X,c)

        return self

    def predict(self, X):
        c = self.h.predict(X)
        y = self.discretizer.inverse_transform(c)
        return y

    def sample(self,X,random_state=None):
        # P(Y = c | X = x)
        p = self.h.predict_proba(X)
        N = len(X)
        C = np.zeros(N)
        for i in range(N):
            C[i] = np.random.choice(len(p[i]), 1, p=p[i])
        y = self.discretizer.inverse_transform(C.reshape(-1,1))
        return y

    def evaluate(self, X, y):
        k = self.discretizer.transform(y.reshape(-1,1)).astype(int)
        p = self.h.predict_proba(X)
        return p[np.arange(len(y)),k[:,0]]

    def sample_eval(self,X,random_state=None):
        y = self.sample(X,random_state)
        return y[:,0], self.evaluate(X, y)

    def get_density(self,x):

        p = self.h.predict_proba(x.reshape(1,-1))[0]
        def f_x(z):
            N = len(z)
            p_z = np.zeros((N))
            for i in range(N):
                k = self.discretizer.transform(z[i].reshape(1,-1)).astype(int)
                p_z[i] = p[k]
            return p_z
        return f_x


def get_estimate(Y_samp,Y_weight,Y_cost,loss="MSE"):
    ''' 
        Obtain an estimate
        ------------------

        Parameters
        ----------

        Y_samp : numpy array of dimension (N,L,M)
            M particles per label/step of L labels, for each of the N instances.

        Y_weight : numpy array of dimension (N,L,M)
            Normalized weights associated with each particle

        Y_cost : numpy array of dimension (N,M) 
            Path cost of each particle (across the full chain)

        loss : string
            e.g., : 'MSE', 'MAP', 'MAE' (see below for full list)

        Returns
        -------

        Y_pred : numpy array of dimension (N,L)
            Predictions/estimator to optimize the given loss. 

        W_pred : numpy array of dimension (N,L) or (N) 
            Associated confidednce on predictions. The dimension depends on 
            the loss metric (i.e., if weight applies to path, or marginals).

        (for MSE: the weights must be normalized per step in the chain).
    '''

    N,L,M = Y_samp.shape

    if loss == "MSE" or loss == "mean_squared_error":
        # Return the weighted mean (average across all particle-paths)
        return np.sum(Y_weight * Y_samp,axis=2), np.mean(Y_weight,axis=2)
    elif loss == "MAP" or loss == "UCF" or loss == "uniform_cost_function":
        # Return the mode (particle-path with biggest weight)
        W = Y_cost
        k = np.argmax(W,axis=1)         # index of max sample per instance
        Yp = np.zeros((N,L))
        Wp = np.zeros((N))
        for i in range(N):
            m_max = k[i]                 # argmax (idx of max weight)
            Wp[i] = W[i,m_max]           # take the max (weight)
            Yp[i] = Y_samp[i,:,m_max]    # take the argmax
        return Yp, Wp
    elif loss == "MAE" or loss == "mean_absolute_error":
        # Return the median particle
        Yp = np.zeros((N,L))
        for i in range(N): 
            for j in range(L):
                Yp[i,j] = np.median(Y_samp[i,j])
        return Yp, "None"
    elif loss == "wMAE" or loss == "weight_mean_absolute_error":
        # Get median weight, and return corresponding particle
        Yp = np.zeros((N,L))
        for i in range(N): 
            for j in range(L):
                k = np.argsort(Y_weight[i,j])[len(Y_weight[i,j])//2]
                Yp[i,j] = Y_samp[i,j,k]
        return Yp, "None"
    elif loss == "WMAE" or loss == "weighted_mean_absolute_error":
        # Return the weighted median
        Yp = np.zeros((N,L))
        for i in range(N): 
            for j in range(L):
                Yp[i,j] = weighted_median(Y_samp[i,j],Y_weight[i,j])
        return Yp, "None"
    else:
        raise Exception("No such loss function: ", loss)


def ESS(w):
    ''' Effective Sample Size (ESS)
    where w is a vector of normalized weights. 
    It means that the ESS of w samples is between 1 and len(w), where 1 is the 
    worst case where only a single sample is non-degenerate. 
    '''
    return 1. / np.sum(w**2)

def normalize(w):
    ''' Normalize w
    and return also the normalizing constant '''
    Z = np.sum(w)
    if Z > 0:
        return w / Z, Z
    elif Z < 0:
        raise Exception("Negative probabilities? %s" % str(w))
    # else: avoid divide by zero
    return w + 1/len(w), 1.

class MonteCarloRegressorChain(RegressorMixin, MultiOutputMixin) :
    ''' Monte Carlo Regressor Chain

    It is required to supply a base_estimator that implements `sample_eval`. 
    '''

    def __init__(self, base_estimator=F_BayesianRidge(), short_chain=True, random_state=None):
        super().__init__()
        self.base_estimator = base_estimator
        self.random_state = random_state
        self.ensemble = None
        self.L = -1
        self.short_chain = short_chain

    def fit(self, X, y):
        ''' Fit the model to data matrix X and targets y. 

        N.B. We ignore chain order for now. 
        '''
        N, self.L = y.shape
        L = self.L
        N, D = X.shape

        self.ensemble = [copy.deepcopy(self.base_estimator) for _ in range(L)]
        XY = np.zeros((N, D + L-1))
        XY[:, 0:D] = X
        XY[:, D:] = y[:, 0:L - 1]
        for j in range(self.L):
            Z_j = XY[:, 0:D + j]
            if self.short_chain:
                Z_j = Z_j[:,-1].reshape(N,1)
            self.ensemble[j].fit(Z_j, y[:, j])
        return self

    def _stack(x, Y, short_chain=False):
        ''' Stack x and Ys together.

            For example, 2 samples, 3 labels:

                x = AB, Y = 123, Z = AB123
                            256      AB456

            Parameters
            ----------
            x : array-like, shape (D)
            Y : array-like, shape (M, j)

            Returns
            -------
            Z : array-like, shape (M,D+j)

        '''
        M, j = Y.shape
        return np.column_stack([np.tile(x.reshape(1,-1),(M,1)), Y])

    def predict_proba(self, X, M=10, epsilon=0, random_state=None):
        ''' This function only returns the weighted samples, i.e., the probability tree. 
        It does not return any estimation. For that, call `predict`. 

        Parameters
        ---------

        X : array-like, shape (N,D)
            Input instances
        M : int
            The number of particles
        epsilon : float
            The threshold for resampling, between 0 and 1,
            set to 0 to disable (since always ESS >= M * 0)
        random_state : int
            For random seed

        Returns
        -------
        (Y_samples, Y_weights) : tuple of array-like, shape (N,L,M),  
                                 (samples and their associated weights)
        '''
        N, D = X.shape

        Y_samples = np.zeros((N,self.L,M))
        W = np.ones((N,self.L,M))
        W_norm = np.ones((N,self.L,M))

        for i in range(N):

            # Initialize filter for i-th instance (x, y_s, w, w_norm)
            x = X[i,:]
            y_s = Y_samples[i]
            w = W[i]
            w_norm = W_norm[i]

            # Across the chain ...
            for j in range(self.L):

                # Package up the conditional
                #Z_j = self._stack(x, y_s[0:j,:].T, self.short_chain)
                Z_j = np.column_stack([np.tile(x.reshape(1,-1),(M,1)), y_s[0:j,:].T])
                if self.short_chain:
                    Z_j = Z_j[:,-1].reshape(-1,1)

                # Sample 
                y_s[j,:], w[j,:] = self.ensemble[j].sample_eval(Z_j, random_state=random_state) 
                # Weight
                w[j,:] = w[j,:] * w[j-1,:]

                # Normalize the weights
                w_norm[j,:], Z = normalize(w[j,:])

                # Resampling (optional step)
                if ESS(w_norm[j,:]) < M * epsilon: 
                    print("*Resampling*")
                    y_s[j,:] = np.random.choice(y_s[j,:],M,p=w_norm[j,:])
                    w[j,:] = 1./M * Z

        return Y_samples, W_norm, W[:,-1,:]

    # This is for efficiency reasons in 'repredict'
    Y_samp = None
    Y_weight = None

    def predict(self, X, M=10, epsilon=0.0, loss=None):
        ''' Returns estimations, for all x in 'X', optimizing 'loss'. 
        '''
        self.Y_samp, self.Y_weight, self.Y_cost = self.predict_proba(X, M, epsilon)

        Yp, Wp = get_estimate(self.Y_samp,self.Y_weight,self.Y_cost,loss=loss)

        return Yp

    def re_predict(self, loss=None):
        ''' Re-estimate without carrying out inference again (for efficiency).
        '''
        assert(self.Y_samp is not None)
        Yp, Wp = get_estimate(self.Y_samp,self.Y_weight,self.Y_cost,loss=loss)


class ParticleFilterRegressorChain(MonteCarloRegressorChain) :
    ''' Particle Filter Probabilistic Regressor Chain

    It extends MonteCarloRegressorChain, for the case where it is not 
    feasible to sample from the given base regressor. In that case, we carry 
    out importance sampling, using another distribution (that is specified). 
    '''

    def __init__(self, transition_fn=F_BayesianRidge(), likelihood_fn=None, random_state=None):
        super().__init__(base_estimator=likelihood_fn, random_state=random_state)
        self.transition_fn = transition_fn
        self.short_chain = False

    def fit(self, X, y):
        ''' fit
        '''
        super().fit(X, y)
        self.f = MonteCarloRegressorChain(base_estimator=self.transition_fn,short_chain=True)
        self.f.fit(X, y)

    def predict_proba(self, X, M=10, epsilon=0, random_state=None):
        ''' As in MonteCarloRegressorChain, but making use of the distribution 
        for taking samples (not the same as the one for evaluating). 
        '''

        f = self.f.ensemble
        g = self.ensemble

        N, D = X.shape

        Y_samples = np.zeros((N,self.L,M))
        W = np.ones((N,self.L,M))
        W_norm = np.ones((N,self.L,M))

        for i in range(N):

            # Initialize new filter ...
            x = X[i,:]
            y_s = Y_samples[i]
            w_norm = W_norm[i]
            w = W[i]

            # Step across the chain ...
            for j in range(self.L):

                Z_j = np.column_stack([np.tile(x.reshape(1,-1),(M,1)), y_s[0:j,:].T])
                U_j = Z_j
                if self.f.short_chain:
                    U_j = Z_j[:,-1].reshape(-1,1)

                # Draw samples y[j] ~ f(x,y[0,...,j-1])
                y_s[j,:], f_y = f[j].sample_eval(U_j, random_state=random_state)

                # Evaluate p(y[m,j]|x,y[m,0,...,j-1])
                l_y = g[j].evaluate(Z_j, y_s[j,:])

                # Compute the transition weights
                w[j,:] = w[j-1,:] * l_y / f_y

                # Normalize the weights
                w_norm[j,:], Z = normalize(w[j,:])

                # Resampling (optional step)
                if ESS(w_norm[j,:]) < M * epsilon:
                    y_s[j,:] = np.random.choice(y_s[j,:],M,p=w_norm[j,:])
                    w[j,:] = 1./ M * Z

        return Y_samples, W_norm, W[:,-1,:]

def run_tests(): 
    # Test with a (N,L,M) = 1 instance, 3 labels, 3 samples
    Y = np.ones((1,3,3))
    Y[0,:,1] = [2, 2, 2]
    Y[0,:,2] = [4, 4, 4]
    W = np.ones((1,3,3))
    W[0,0,:] = [0.9, 0.1, 0.9]
    W[0,1,:] = [0.8, 0.8, 0.7]
    W[0,2,:] = [0.3, 0.4, 0.5]
    w = np.prod(W[0,:,:],axis=0).reshape(1,-1)
    W = W / np.sum(W.T,axis=0)
    print(Y)
    print(W)
    print(w.shape)
    true_results = {
            "wMAE" :  [1, 1, 2],
            "MAP" :  [4, 4, 4],
            "MSE" :  [2.47,2.26,2.58],
        }
    for s in "wMAE", "MAP", "MSE": 
        yp, wp = get_estimate(Y,W,w,loss=s)
        #assert(yp, 
        print(s, yp, true_results[s])

def demo():
    from utils_metrics import uniform_cost_function as UCF
    from sklearn.metrics import mean_absolute_error as MAE

    XY = np.genfromtxt('data/synth_2.csv', skip_header=0, delimiter=",")
    #XY = np.genfromtxt('data/andro_6.csv', skip_header=0, delimiter=",")
    from sklearn import preprocessing
    XY = preprocessing.scale(XY)
    L = 2
    np.random.shuffle(XY)
    N,DL = XY.shape
    D = DL - L
    X = XY[:,0:D]
    Y = XY[:,-L:]

    # Test baseline
    from skmultiflow.meta import RegressorChain as RC
    h = RC()
    h.fit(X, Y)
    Ypred = h.predict(X)
    print(str(h.__class__.__name__))
    print("---------------")
    print("MSE", np.mean(Ypred - Y)**2)
    #print("MAE", MAE(Y,Ypred))
    print("MAP", UCF(Y,Ypred))
    print(" ")

    # Test MC methods
    for g in [F_BayesianRidge(), F_Histogram_Classifier(k=10), F_Kernel(kernel='rbf',gamma=0.75)]:
        h = MonteCarloRegressorChain(g)
        h.fit(X, Y)
        Ypred = h.predict(X,10,loss='MSE')
        print(str(h.__class__.__name__))
        print("---------------")
        print("MSE", np.mean(Ypred - Y)**2)
        #print("MAE", MAE(Y,Ypred))
        print("MAP", UCF(Y,Ypred))
        print(" ")

    # Test PF methods
    for (g,f) in zip([F_BayesianRidge(),F_Kernel(kernel='rbf',gamma=0.75), F_Histogram_Classifier(k=10), F_Kernel(kernel='rbf',gamma=0.75)],[F_BayesianRidge(),F_BayesianRidge(),F_BayesianRidge(),F_Histogram_Classifier(k=10)]):
        h = ParticleFilterRegressorChain(g,f)
        h.fit(X, Y)
        Ypred = h.predict(X,10,loss='MAP')
        print(str(h.__class__.__name__))
        print("---------------")
        print("MSE", np.mean(Ypred - Y)**2)
        #print("MAE", MAE(Y,Ypred))
        print("MAP", UCF(Y,Ypred))
        print(" ")

if __name__ == '__main__':
    run_tests()
    #demo()
