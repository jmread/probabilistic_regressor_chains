import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter

from sklearn import preprocessing

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.neural_network import MLPClassifier

from utils_plot import gen_cplot
from utils_plot import make_density

from probabilistic_regressor_chains import MonteCarloRegressorChain, ParticleFilterRegressorChain
from probabilistic_regressor_chains import F_BayesianRidge, F_Histogram_Classifier, F_Kernel
from probabilistic_regressor_chains import get_estimate

from skmultiflow.meta import RegressorChain as RC


'''
    Run the methods on a dataset, display the trajectories for a single test instance.
'''

# Set parameters

np.random.seed(4)
i_select = 0
M = 50

datasets = [
    'synth_2.csv', 
    #'andro_6.csv', 
    #'traffic_30.csv', 
    ]

# For each dataset ...

for fname in datasets:

    # Process dataset

    dname =  fname.split('_')[0]
    L = int(fname.split('_')[1].split('.')[0])

    XY = np.genfromtxt('data/'+str(fname), skip_header=1, delimiter=",")
    XY = XY + np.random.randn(*XY.shape) * 0.1
    np.random.shuffle(XY)
    XY = preprocessing.scale(XY)
    N,DL = XY.shape
    D = DL - L
    X = XY[:,0:D]
    Y = XY[:,-L:] 

    n = int(N*8/9)
    X_train = X[0:n]
    Y_train = Y[0:n]
    X_test = X[n:]
    Y_test = Y[n:]

    # Set up a learner

    gamma = 0.1
    h = ParticleFilterRegressorChain(F_Kernel(kernel='rbf',gamma=gamma),F_Kernel(kernel='rbf',gamma=gamma))
    mtype = "PFKK"
    h = MonteCarloRegressorChain(F_Histogram_Classifier(ExtraTreesClassifier(n_estimators=50),k=20))
    mtype = "d"
    h = MonteCarloRegressorChain(F_BayesianRidge())
    mtype = "B"
    h = MonteCarloRegressorChain(F_Kernel(kernel='rbf',gamma=gamma))
    mtype = "K"

    # Fit the model
    h.fit(X_train,Y_train)         

    # Predict (for i_select-th instance only)
    (Ysamp, Yweight, Ypcost) = h.predict_proba(X_test[i_select].reshape(1,-1),M)
    Ypred_MSE, Wpred_MSE = get_estimate(Ysamp,Yweight,Ypcost,loss="MSE")
    Ypred_MAP, Wpred_MAP = get_estimate(Ysamp,Yweight,Ypcost,loss="MAP")
    Ypred_MAE, Wpred_MAE = get_estimate(Ysamp,Yweight,Ypcost,loss="MAE")

    _,L,M = Yweight.shape

    ###################### PLOT 2: Plot paths (j vs y_j) ##############################################

    plt.figure()
    plt.plot(Y_test.T[0:10],'k-',alpha=0.2) #,label=r'$\{{\bf y}\}_{i=1}^N$')
    plt.plot(Y_test[0,:],'k-',label=r'$\{{\bf y}_i\}_{i=1}^N$',alpha=0.2)  # <-- just to get the legend label
    plt.plot(Ysamp[0],'m-', linewidth=1, alpha=0.6) # ,label=r'$\{{\bf y}^{(m)}\}_{m=1}^M$'
    plt.plot(Ysamp[0,:,0],'m-',label=r'$\{{\bf y}^{(m)}_i\}_{m=1}^M$',alpha=0.6)  # <-- just to get the legend label
    plt.plot(Y_test[i_select].T,'k-', linewidth=3, label=r'${\bf y}|x$')
    plt.plot(Ypred_MSE[0],'b-', linewidth=3, label=r'${\bf \hat y}_{\sf MSE}$')
    plt.plot(Ypred_MAP[0],'r-', linewidth=3, label=r'${\bf \hat y}_{\sf MAP}$')

    min_y, max_y = plt.ylim()
    for j in range(L):
        f_yp = make_density(Ysamp[0,j,:])
        f_y = make_density(Y[j,:])
        x = np.linspace(min_y,max_y,100)
        #plt.plot(density(x,f_y)+j, x, 'k--', label = "$p(y_j|x)$" if (j == 0) else "")
        plt.plot(f_yp(x)*0.5+j, x, 'k--', label = "$\hat p(y_j|x)$" if (j == 0) else "")

    plt.ylabel(r'$y_j$')
    #plt.xlabel(r'$j$')
    ticks = ["$j=%d$" % (j+1) for j in range(L)]
    plt.xticks(range(0,L),ticks)
    plt.legend()
    plt.grid(True,axis='x',linestyle='-',linewidth=2, alpha=1, color='k')
    plt.tight_layout()
    #plt.savefig("../fig/demo_%s-%s_step_3.pdf" % (dname,mtype), format="pdf")
    plt.show()

    ###################### PLOT 1: Plot 1) x vs y_1 and 2) y_1 vs y_2 #######################################

    ###################### j = 0 : Plotting p(y_1 | x) 
    j = 0
    x = X_train[:,0]
    y = Y_train[:,j]
    f_1 = make_density(y) 
    fig1, ax1 = gen_cplot(x,y,f_1,h.ensemble[j].get_density(X_test[i_select]))
    ax1.scatter(x, y, c='k', label=("$\{x^{(i)},y^{(i)}_%d\}_{i=1}^N$" % (j+1)),alpha=0.2)
    for m in range(M):
        ax1.plot(X_test[i_select,j], Ysamp[0,j,m],'mo',markersize=Yweight[0,j,m]*M*10,mfc='none',label="$\{x,y^{(m)}_1\}_{m=1}^{M}$" if m == 0 else "")
    ax1.legend()
    ax1.set_xlabel(r'$x$')
    ax1.set_ylabel(r'$y_1$')
    ax1.set_xticks([], [])
    plt.tight_layout()
    #plt.savefig("../fig/demo_%s-%s_step_1.pdf" % (dname,mtype), format="pdf")
    plt.show()

    ###################### j = 1 : Plotting p(y_2 | y_1) 
    j = 1
    x = Y_train[:,j-1]
    y = Y_train[:,j]
    Z_test_i = np.column_stack([X_test[i_select], Y_test[i_select,j-1]])[0]
    # N.B. short_chain (make sure to set this option)
    Z_test_i = Z_test_i[-1]
    f_2 = make_density(y)
    fig2, ax2 = gen_cplot(x,y,f_2,h.ensemble[j].get_density(Z_test_i))
    ax2.scatter(x, y, c='k', label=("$\{y_{%d}^{(i)},y_{%d}^{(i)}\}_{i=1}^N$" % (j, j+1)), alpha=0.2)
    for m in range(M):
        ax2.plot(Ysamp[0,j-1,m], Ysamp[0,j,m],'mo',markersize=Yweight[0,j,m]*M*10,mfc='none',label="$\{ y^{(m)}_1, y^{(m)}_2\}_{m=1}^{M}$" if m == 0 else "")
    ax2.plot(Ypred_MSE[0,0], Ypred_MSE[0,1],'bx',markersize=10,linewidth=3,mfc='none',label='$\hat y_{MSE}$') #,label="$\{ y^{(m)}_1, y^{(m)}_2\}_{m=1}^{M}$"
    ax2.plot(Ypred_MAP[0,0], Ypred_MAP[0,1],'rx',markersize=10,linewidth=3,mfc='none',label="$\hat y_{MAP}$") #,label="$\{ y^{(m)}_1, y^{(m)}_2\}_{m=1}^{M}$"
    ax2.set_ylabel(r'$y_2$')
    ax2.set_xlabel(r'$y_1$')
    ax2.set_xticks([], [])
    ax2.legend()
    plt.tight_layout()
    #plt.savefig("../fig/demo_%s-%s_step_2.pdf" % (dname,mtype), format="pdf")
    plt.show()
