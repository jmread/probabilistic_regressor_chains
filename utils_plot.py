import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter

from scipy.stats import gaussian_kde

from sklearn.neighbors import KernelDensity as KDE

'''
    Utils for plotting conditional densities, specific to regressor chains.
'''

def gen_cplot(x,y,p,px=None):

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.8
    bottom_h = left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histy = [left_h, bottom, 0.2, height]

    # start with a rectangular Figure
    fig = plt.figure(figsize=(6, 5))

    axScatter = plt.axes(rect_scatter)
    axHisty = plt.axes(rect_histy)

    # no labels
    axHisty.yaxis.set_major_formatter(NullFormatter())

    # the scatter plot:
    axScatter.set_xlim((min(x), max(x)))
    axScatter.set_ylim((min(y), max(y)))

    #axHisty.hist(y, bins=bins, orientation='horizontal')
    xxx = np.linspace(min(y),max(y),200)
    axHisty.plot(p(xxx),xxx,'k-',label="$p(y)$", alpha=0.2)
    if px is not None: 
        axHisty.plot(px(xxx),xxx,'m--',label="$\\tilde p(y|x)$")
    axHisty.legend()

    axHisty.set_ylim(axScatter.get_ylim())

    return fig, axScatter

def make_density(x,bandwidth=0.5):
    '''
        Returns kde p, such that we may plot x vs p
    '''
    f = KDE(kernel='gaussian',bandwidth=bandwidth).fit(x.reshape(-1,1))
    def kde(x):
        p = np.zeros_like(x)
        for i in range(len(x)):
            p[i] = np.exp(f.score(x[i].reshape(-1,1)))
        return p
    return kde

#def make_density_v2(x):
#    return gaussian_kde(x)

if __name__ == '__main__':
    # random data
    x = np.random.randn(1000)
    y = np.random.randn(1000)

    plt.figure()
    f_1 = make_density(x)
    #f_2 = make_density_v2(x)
    xxx = np.linspace(-3,3,100)
    plt.plot(xxx,f_1(xxx))
    #plt.plot(xxx,f_2(xxx))
    plt.show()

