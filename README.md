# Probabilistic Regressor Chains

This project contains code relating to different methods of *Probabilistic Regressor Chains*. It is intended to be eventually integrated with the framework [Scikit-Multiflow](https://github.com/scikit-multiflow/scikit-multiflow) (by which time, documentation will be extended, tests added, etc. Until then, this repository can be considered prototype code). Learners are designed close to the style of Scikit-Multiflow and by extension Scikit-Learn. 

A demo/test of the code can be run with: 

```sh
	python probabilistic_regressor_chanis.py
```

Some plots which are useful for developing/debugging/demonstration can be optained with:

```sh
	python run_demo.py
```

It will produce the following plots in the directory `.fig/`: 

![Demo1](./fig/demo_synth-K_step_1.pdf)  
![Demo2](./fig/demo_synth-K_step_2.pdf)  
![Demo3](./fig/demo_synth-K_step_3.pdf)  

N.B. Some of the code requires also Scikit-Multiflow for the baseline method (regrossor chains) to run. 
